#define DOCTEST_CONFIG_NO_SHORT_MACRO_NAMES
#include <calsig/calsig.h>
#include <calsig/version.h>
#include <doctest/doctest.h>

#include <fstream>
#include <iostream>
#include <string>

namespace {
  auto basler = std::filesystem::path(
      "/media/hubert/disk1/bl8/f2/wetransfer_basl_select_sig_2338_2022-01-27_1118/"
      "BASL_SELECT_sig_2338");
  auto rosbag = std::filesystem::path(
      "/media/hubert/disk1/bl8/f0/wetransfer_sig-1_2022-01-27_1049/rosbag/rosbag_0.db3");
}  // namespace

using namespace calsig;
// DOCTEST_TEST_CASE("NamesToTimestamps") {
//   auto timeimage
//       = NamesToTimestamps("/media/hubert/disk1/2021_05_18_RECORD16_15_45_cut_voie1/BASLER");
//   for (auto &&i : timeimage.timestamp) {
//     std::cout << std::setprecision(13) << i << std::endl;
//   }
// }
// DOCTEST_TEST_CASE("BagToQuat") {
//   auto bag0x = calsig::FromBagTo0x("/home/hubert/Downloads/2021_05_18_RECORD15_15_03_cut/rosbag",
//                                    "/sensor_2/filter");
// }
// DOCTEST_TEST_CASE("Calsig version") {
//   static_assert(std::string_view(CALSIG_VERSION) == std::string_view("1.0"));
//   CHECK(std::string(CALSIG_VERSION) == std::string("1.0"));
// }

// DOCTEST_TEST_CASE("Rails") {
//   DOCTEST_SUBCASE("scenario.json") {
//     calsig::Scenario_To_Rails("test/data/scenarios/Voie1_Depart-V2A_Arrivee-V1_Railenium.json",
//                               "test/data/scenarios/catalog.json");
//   }
// }

// todo: standalones
// DOCTEST_TEST_CASE("ClickPoint") {
//   // auto timeimage{NamesToTimestamps(basler)};
//   // auto images = SortFolder(basler);
//   auto folder_image
//       = "/media/hubert/disk1/bl8/f0/wetransfer_sig-1_2022-01-27_1049/BASLER_select_2326";
//   auto bagpath =
//   "/media/hubert/disk1/bl8/f0/wetransfer_sig-1_2022-01-27_1049/rosbag/rosbag_0.db3"; auto
//   time_image = calsig::NamesToTimestamps(folder_image);
//   // todo: automatic sensor selection
//   auto bag0x = calsig::FromBagTo0x(bagpath, "/sensor_2/filter");
//   auto scenario = "test/data/scenarios/Voie1_Depart-V2A_Arrivee-V1_Railenium.json";
//   auto catalog = "test/data/scenarios/catalog.json";
//   auto data = calsig::Scenario_To_Rails(scenario, catalog);
//   // todo: itf
//   auto targets = calsig::ImagesToTargets(time_image.imagepath, time_image, bag0x, data);
//   auto targetfile{std::ofstream("cab1_target.txt")};
//   // todo: vector of struct
//   for (size_t i = 0; i < targets.imagepath.size(); i++) {
//     targetfile << targets.imagepath[i] << " " << targets.signal_id[i] << " "
//                << targets.targets[i].transpose() << std::endl;
//   }
// }

// DOCTEST_TEST_CASE("Image To T0x") {
//   auto time_image{NamesToTimestamps(basler)};
//   auto bag0x{calsig::FromBagTo0x(rosbag, "/sensor_2/filter")};
//   auto T0x = calsig::ImageToT0x(time_image.imagepath[10], time_image, bag0x);
//   std::cout << T0x << std::endl;
// }

DOCTEST_TEST_CASE("best txc") {
  auto scenario = "test/data/scenarios/Voie1_Depart-V2A_Arrivee-V1_Railenium.json";
  auto catalog = "test/data/scenarios/catalog.json";

  calsig::BestTxc(scenario, catalog, basler, rosbag);
}