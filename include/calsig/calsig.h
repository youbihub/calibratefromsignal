#pragma once
#include <Eigen/Geometry>
#include <filesystem>
#include <vector>

#include "camera/camera.h"
#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include "rail/rail.h"

namespace calsig {

  struct TimeImage {
    std::vector<double> timestamp;
    std::vector<std::string> imagepath;
  };
  struct TargetImage {
    std::vector<std::filesystem::path> imagepath;
    std::vector<Eigen::Vector2d> targets;
    std::vector<std::string> signal_id;
  };

  struct Bag0x {
    std::vector<double> time;
    std::vector<Eigen::Quaterniond> quat;
    std::vector<Eigen::Vector3d> lla;
    std::vector<Eigen::Vector3d> xyz;
    std::vector<Eigen::Matrix4d> T0x;
  };

  struct UmapData {
    Eigen::Vector3d signal_ecef;
    rail::Rail<double> rail;
  };

  auto Scenario_To_Rails(const std::filesystem::path& path_scenario,
                         const std::filesystem::path& path_catalog)
      -> std::unordered_map<std::string, UmapData>;

  auto SortFolder(const std::filesystem::path& folder) -> std::vector<std::filesystem::path>;
  auto NameToTimestamp(const std::filesystem::path& path) -> double;
  auto NamesToTimestamps(const std::filesystem::path& folder) -> TimeImage;
  auto FromBagTo0x(const std::string& rosbag_path, const std::string& topic_name) -> Bag0x;

  auto ImagesToTargets(const std::vector<std::string>& paths, const TimeImage& time_image,
                       const Bag0x& bag0x, const std::unordered_map<std::string, UmapData>& data)
      -> TargetImage;

  auto ImageToT0x(const std::filesystem::path& path, const TimeImage& time_image,
                  const Bag0x& bag0x) -> int;

  struct CostFunctor {
    CostFunctor(const Eigen::Vector3d& p_ecef, const Eigen::Matrix4d& t_ecef_x,
                const camera::Calibration& calib, const Eigen::Vector2d pix_tgt)
        : P_ecef(p_ecef), T_ecef_x(t_ecef_x), calibration(calib), pix_target(pix_tgt) {}
    template <typename T>
    bool operator()(const T* const xyz, const T* const aa, T* residual) const {
      auto t_xc{Eigen::Map<const Eigen::Matrix<T, 3, 1>>(xyz)};
      // std::cout << t_xc << std::endl;
      auto Rxc{Eigen::Matrix<T, 3, 3>{}};
      ceres::AngleAxisToRotationMatrix(aa, Rxc.data());
      auto Txc{Eigen::Matrix<T, 4, 4>{}};
      Txc(Eigen::seqN(0, 3), Eigen::seqN(0, 3)) = Rxc;
      Txc.col(3) << (T)t_xc(0), (T)t_xc(1), (T)t_xc(2), (T)1.;
      Txc(3, Eigen::seqN(0, 3)) << (T)0., (T)0., (T)0.;
      auto p_ecef1{
          (Eigen::Matrix<T, 4, 1>{} << (T)P_ecef(0), (T)P_ecef(1), (T)P_ecef(2), (T)1).finished()};
      auto p_c{Txc.inverse() * T_ecef_x.inverse() * p_ecef1};
      if (p_c(0) < (T)0) {
        return false;
      }

      auto pc3{(Eigen::Matrix<T, 3, 1>{} << (T)p_c(0), (T)p_c(1), (T)p_c(2)).finished()};
      // std::cout << pc3.transpose() << std::endl;
      auto p_ij = camera::ProjectPoint(pc3, calibration);
      auto res{pix_target - p_ij};
      residual[0] = res(0);
      residual[1] = res(1);
      return true;
    }
    Eigen::Vector3d P_ecef;
    Eigen::Matrix4d T_ecef_x;
    camera::Calibration calibration;
    Eigen::Vector2d pix_target;
  };

  auto BestTxc(const std::filesystem::path& scenario, const std::filesystem::path& catalog,
               const std::filesystem::path& folder_basler,
               const std::filesystem::path& folder_rosbag) -> Eigen::Matrix4d;

}  // namespace calsig
