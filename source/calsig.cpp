#include <calsig/calsig.h>
// #include <fmt/format.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <optional>
#include <stdexcept>

#include "ceres/local_parameterization.h"
#include "geometry_msgs/msg/quaternion_stamped.hpp"
#include "geometry_msgs/msg/vector3_stamped.hpp"
#include "lambertgps/lambertgps.h"
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "rail/rail.h"
#include "rclcpp/serialization.hpp"
#include "rclcpp/serialized_message.hpp"
#include "rosbag2_cpp/reader.hpp"
#include "rosbag2_cpp/readers/sequential_reader.hpp"
#include "rosbag2_storage/storage_options.hpp"

namespace fs = std::filesystem;

auto calsig::NameToTimestamp(const std::filesystem::path& path) -> double {
  if (!fs::exists(path)) {
    throw std::runtime_error("Image path does not exist!");
  }
  std::stringstream timeStr;
  std::tm tm = {};
  auto imageName{path.filename().string()};
  timeStr << imageName.substr(0, 19);
  // Get a time from the string
  const std::string FORMAT = "%Y_%m_%d_%H_%M_%S";
  timeStr >> std::get_time(&tm, FORMAT.c_str());
  const auto UTC_OFFSET = 1;
  tm.tm_hour += UTC_OFFSET;
  // compute time ffilenamerom epoch
  auto time = std::mktime(&tm);
  // Stamp
  double doubletime = static_cast<int32_t>(time);
  // milliseconds to seconds
  doubletime += stoi(imageName.substr(20, 3)) * 1e-3;

  return doubletime;
}
auto calsig::SortFolder(const std::filesystem::path& folder) -> std::vector<std::filesystem::path> {
  if (!fs::exists(folder)) {
    throw std::runtime_error("Image folder does not exist!");
  }
  auto files = std::vector<std::filesystem::path>();
  for (auto& p : fs::directory_iterator(folder)) {
    files.push_back(p.path());
  }
  std::sort(files.begin(), files.end());
  return files;
}

auto calsig::NamesToTimestamps(const fs::path& folder) -> TimeImage {
  if (!fs::exists(folder)) {
    throw std::runtime_error("Image folder does not exist!");
  }
  auto time_image{TimeImage{}};
  for (auto& p : fs::directory_iterator(folder)) {
    std::stringstream timeStr;
    std::tm tm = {};
    auto imageName{p.path().filename().string()};
    timeStr << imageName.substr(0, 19);
    // Get a time from the string
    const std::string FORMAT = "%Y_%m_%d_%H_%M_%S";
    timeStr >> std::get_time(&tm, FORMAT.c_str());
    const auto UTC_OFFSET = 1;
    tm.tm_hour += UTC_OFFSET;
    // compute time from epoch
    auto time = std::mktime(&tm);
    // std::cout << timeStr.str() << std::endl;
    auto doubletime{0.};
    // Stamp
    doubletime = static_cast<int32_t>(time);
    // milliseconds to seconds
    doubletime += stoi(imageName.substr(20, 3)) * 1e-3;
    time_image.timestamp.push_back(doubletime);
    time_image.imagepath.push_back(p.path().string());
  };
  std::sort(time_image.timestamp.begin(), time_image.timestamp.end());
  std::sort(time_image.imagepath.begin(), time_image.imagepath.end());

  return time_image;
}

auto calsig::FromBagTo0x(const std::string& rosbag_path, const std::string& topic_name) -> Bag0x {
  using Q = geometry_msgs::msg::QuaternionStamped;
  using V = geometry_msgs::msg::Vector3Stamped;
  rosbag2_cpp::Reader reader(std::make_unique<rosbag2_cpp::readers::SequentialReader>());
  rosbag2_storage::StorageOptions storage_options;
  const auto& uri{rosbag_path};
  storage_options.uri = uri;
  storage_options.storage_id = "sqlite3";
  rosbag2_cpp::ConverterOptions converter_options{"cdr", "cdr"};
  reader.open(storage_options, converter_options);
  auto storage_filter{rosbag2_storage::StorageFilter{}};
  storage_filter.topics.push_back(topic_name + "/quaternion");
  storage_filter.topics.push_back(topic_name + "/positionlla");
  // reader.set_filter({{topic_name + "/quaternion", topic_name + "/positionlla"}});
  reader.set_filter(storage_filter);
  auto output{calsig::Bag0x{}};
  rclcpp::Serialization<Q> serialization_quat;
  rclcpp::Serialization<V> serialization_lla;
  while (reader.has_next()) {
    auto bag_message{reader.read_next()};
    rclcpp::SerializedMessage extracted_serialized_msg(*bag_message->serialized_data);
    if (bag_message->topic_name == topic_name + "/quaternion") {
      Q q;
      serialization_quat.deserialize_message(&extracted_serialized_msg, &q);
      output.quat.emplace_back(
          Eigen::Quaterniond{q.quaternion.w, q.quaternion.x, q.quaternion.y, q.quaternion.z});
      output.time.push_back(q.header.stamp.sec + q.header.stamp.nanosec * 1e-9);
    } else if (bag_message->topic_name == topic_name + "/positionlla") {
      V v;
      serialization_lla.deserialize_message(&extracted_serialized_msg, &v);
      output.lla.emplace_back(Eigen::Vector3d{{v.vector.x, v.vector.y, v.vector.z}});
    }
  }
  for (auto lla : output.lla) {
    lla(Eigen::seq(0, 1)) *= M_PI / 180.;
    output.xyz.push_back(lamwgs::Wgs84ToEcef(lla));
  }

  for (size_t i = 0; i < output.time.size(); i++) {
    auto T0x = Eigen::Matrix4d::Identity().eval();
    T0x.topRightCorner<3, 1>() = output.xyz[i];
    auto R_0_NWU = rail::NWU(output.xyz[i]);
    auto R_NWU_X = output.quat[i].toRotationMatrix();
    T0x.topLeftCorner<3, 3>() = R_0_NWU * R_NWU_X;
    output.T0x.push_back(T0x);
  }

  return output;
}

static void onMouse(int event, int x, int y, int, void* input_ptr) {
  auto& input = *(Eigen::Vector2d*)input_ptr;
  switch (event) {
    case cv::EVENT_LBUTTONDOWN:
      input << x, y;
      std::cout << "point: " << input.transpose() << std::endl;
      break;
  }
}

auto calsig::ImagesToTargets(const std::vector<std::string>& paths, const TimeImage& time_image,
                             const Bag0x& bag0x,
                             const std::unordered_map<std::string, UmapData>& data) -> TargetImage {
  auto output = TargetImage{};
  for (auto&& path : paths) {
    auto idx = ImageToT0x(path, time_image, bag0x);
    auto signal_target = [&](const auto& p, const auto& dir_x) -> std::optional<std::string> {
      for (auto&& [signal, umap] : data) {
        auto vec = (umap.signal_ecef - p).eval();
        auto dist = vec.dot(dir_x);

        if (dist > 10 && dist < 200) {
          return signal;
        }
      }
      return std::nullopt;
    };
    auto signal = signal_target(bag0x.xyz[idx], bag0x.T0x[idx](Eigen::seqN(0, 3), 0));
    if (!signal.has_value()) {
      continue;
    }

    auto image = cv::imread(path);
    cv::imshow("image", image);
    auto callback_input{(Eigen::Vector2d{} << -1, -1).finished()};
    cv::setMouseCallback("image", onMouse, &callback_input);
    cv::waitKey(0);
    if (callback_input(0) < 0) {
      continue;
    }

    output.targets.push_back(callback_input);  // todo: all these in onMouse
    output.imagepath.push_back(path);
    output.signal_id.push_back(signal.value());
  }
  return output;
}

// todo: return some T0x instead? reshape t0x as vector of struct
auto calsig::ImageToT0x(const std::filesystem::path& path, const TimeImage& /* time_image */,
                        const Bag0x& bag0x) -> int {
  // auto idx = std::find(time_image.imagepath.begin(), time_image.imagepath.end(), path)
  //            - time_image.imagepath.begin();
  // auto& target_time{time_image.timestamp[idx]};
  auto imstamp = NameToTimestamp(path);
  // todo: cubic interp or some other things
  auto dtime = bag0x.time;
  for (size_t i = 0; i < bag0x.time.size(); i++) {
    dtime[i] = abs(bag0x.time[i] - imstamp);
  }

  auto min_ele = std::min_element(dtime.begin(), dtime.end());
  auto idx_min = min_ele - dtime.begin();
  std::cout << bag0x.time[idx_min] << std::endl;
  // auto T0x{bag0x.T0x[idx_min]};

  return idx_min;
}

auto calsig::Scenario_To_Rails(const std::filesystem::path& path_scenario,
                               const std::filesystem::path& path_catalog)
    -> std::unordered_map<std::string, UmapData> {
  auto read_json = [&](const auto& p) {
    auto ifile = std::ifstream(p);
    auto json = nlohmann::json{};
    ifile >> json;
    return json;
  };
  auto scenario = read_json(path_scenario);
  auto signals = scenario["signals"].get<std::vector<std::string>>();
  auto catalog = read_json(path_catalog);
  auto data = std::unordered_map<std::string, UmapData>{};
  auto node2lla = [](const nlohmann::json& node) {
    auto lla = Eigen::Vector3d();
    auto altitude = node["altitude"].get<double>();
    auto latitude = node["latitude"].get<double>() * M_PI / 180;
    auto longitude = node["longitude"].get<double>() * M_PI / 180;
    lla << latitude, longitude, altitude;
    return lla;
  };
  for (auto&& signal : signals) {
    rail::Rail<double> rail;
    auto nodes = catalog[signal]["nodes"];
    auto lla{Eigen::Matrix3Xd{3, 20}};
    int k = 0;
    for (auto&& node : nodes) {
      lla.col(k++) = node2lla(node);
    }
    if (k < 20) {
      continue;
    }
    auto xyz = lamwgs::Wgs84ToEcef(lla);
    auto rail_pb = rail::RailProblem(xyz.cols());
    // rail_pb.options.minimizer_progress_to_stdout = true;
    rail = rail_pb.Xyz2rail(xyz);
    auto siglla = node2lla(catalog[signal]);
    // todo: map id->rail
    data[signal] = UmapData{.signal_ecef = lamwgs::Wgs84ToEcef(siglla), .rail = rail};
  }

  return data;
}

auto calsig::BestTxc(const std::filesystem::path& scenario, const std::filesystem::path& catalog,
                     const std::filesystem::path& folder_basler,
                     const std::filesystem::path& folder_rosbag) -> Eigen::Matrix4d {
  // rails
  auto data = Scenario_To_Rails(scenario, catalog);
  auto time_image = NamesToTimestamps(folder_basler);
  auto bag0x = calsig::FromBagTo0x(folder_rosbag, "/sensor_2/filter");
  // init
  auto t_xc{Eigen::Vector3d::Zero().eval()};
  auto aa{Eigen::Vector3d::Zero().eval()};
  auto calib{camera::Calibration("test/basler.json")};
  // "altitude" : 306.46, "latitude" : 49.4957018, "longitude" : 5.7419741,
  // auto tgt_ecef{lamwgs::Wgs84ToEcef(
  //     (Eigen::Vector3d{} << 49.4675126 * M_PI / 180., 5.6298307 * M_PI / 180., 274.045)
  //         .finished())};
  ceres::Problem problem;
  auto ifile = std::ifstream("cab1_target.txt");
  auto path = std::string{};
  auto id = std::string{};
  double tx, ty;
  std::vector<ceres::ResidualBlockId> res_bid;
  auto t_ecef_xs = std::vector<Eigen::Matrix4d>();
  auto path_s = std::vector<std::string>();
  while (ifile >> path >> id >> tx >> ty) {
    auto pix_tgt{(Eigen::Vector2d{} << tx, ty).finished()};
    auto& rail = data[id].rail;
    auto closest_problem{rail::ClosestProblem<rail::Rail<double>>(rail, 200.)};
    path = path.substr(1, path.length() - 2);
    auto idx{calsig::ImageToT0x(path, time_image, bag0x)};
    auto pnav_ecef{bag0x.xyz[idx]};
    auto qnwunav{bag0x.quat[idx]};
    auto s = closest_problem.Solve(pnav_ecef);
    if (s > 200) continue;  // todo: should be useless with correct dataprep

    auto t_ecef_x = rail::TEcefNavUmap(rail::PointOnRail(rail, s), qnwunav);
    {
      path_s.push_back(path);
      t_ecef_xs.push_back(t_ecef_x);
    }
    auto tgt_ecef = data[id].signal_ecef;
    std::cout << "ecef_sig: " << tgt_ecef.transpose() << std::endl;
    std::cout << "ecef_xsens:\n" << t_ecef_x << std::endl;

    ceres::CostFunction* cost_function = new ceres::AutoDiffCostFunction<CostFunctor, 2, 3, 3>(
        new CostFunctor(tgt_ecef, t_ecef_x, calib, pix_tgt));
    auto id = problem.AddResidualBlock(cost_function, nullptr, t_xc.data(), aa.data());
    res_bid.push_back(id);
    auto residuals = Eigen::Vector2d();
    problem.EvaluateResidualBlock(id, false, nullptr, residuals.data(), nullptr);
    std::cout << path << "\tres: " << residuals.transpose() << std::endl;
  }
  ceres::Solver::Options options;
  options.linear_solver_type = ceres::LinearSolverType::SPARSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = true;
  ceres::Solver::Summary summary;
  Solve(options, &problem, &summary);
  std::cout << summary.FullReport() << std::endl;
  std::cout << aa.transpose() << std::endl;
  auto rot{Eigen::Matrix3d{}};
  ceres::AngleAxisToRotationMatrix(aa.data(), rot.data());
  std::cout << "rotation:\n" << rot << std::endl;
  std::cout << "translation:\n" << t_xc.transpose() << std::endl;

  for (int q = 1; auto&& i : res_bid) {
    auto res{Eigen::Vector2d{}};
    problem.EvaluateResidualBlock(i, false, nullptr, res.data(), nullptr);
    std::cout << q++ << ": " << res.transpose() << std::endl;
  }

  auto ofile = std::ofstream("image_Tcs.txt");
  auto Rxc{Eigen::Matrix3d{}};
  ceres::AngleAxisToRotationMatrix(aa.data(), Rxc.data());
  auto Txc = Eigen::Matrix4d::Identity().eval();
  Txc(Eigen::seqN(0, 3), Eigen::seqN(0, 3)) = Rxc;
  Txc.col(3) << t_xc, 1.;
  Txc(3, Eigen::seqN(0, 3)) << 0., 0., 0.;
  // todo: for shaper standalone
  // auto Tcx = Txc.inverse().eval();
  // for (size_t k = 0; k < t_ecef_xs.size(); k++) {
  //   auto Tx0 = t_ecef_xs[k].inverse().eval();
  //   auto T0s = rail::TEcefSignal(tgt_ecef, rail.apparatus[0]);
  //   auto Tcs = Tcx * Tx0 * T0s;
  //   ofile << path_s[k] << " " << Tcs.reshaped().transpose() << '\n';
  //   std::cout << Tcs << "\n----\n" << std::endl;
  // }

  return Txc;
}