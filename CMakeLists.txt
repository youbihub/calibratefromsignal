cmake_minimum_required(VERSION 3.14 FATAL_ERROR)

# ---- Project ----

# Note: update this to your new project's name and version
project(
  Calsig
  VERSION 1.0
  LANGUAGES CXX
)

# ---- Include guards ----

if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
  message(
    FATAL_ERROR
      "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there."
  )
endif()

# ---- Add dependencies via CPM ----
# see https://github.com/TheLartians/CPM.cmake for more info

include(cmake/CPM.cmake)

# PackageProject.cmake will be used to make our target installable
CPMAddPackage("gh:TheLartians/PackageProject.cmake@1.7.0")
find_package(Eigen3 REQUIRED)
find_package(OpenCV REQUIRED)
find_package(Ceres REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rosbag2_cpp REQUIRED)
CPMAddPackage(
  NAME fmt
  GIT_TAG 7.1.3
  GITHUB_REPOSITORY fmtlib/fmt
  OPTIONS "FMT_INSTALL YES" # create an installable target
)
# CPMAddPackage(
#   NAME LambertGps
#   GIT_REPOSITORY git@gitlab.com:hubert-andre/lambert93-wgs84.git
#   GIT_TAG master
# )
CPMAddPackage(
  NAME Rail
  GIT_REPOSITORY git@gitlab.com:signal-detection-system-coding-team/micro-mapping.git
  GIT_TAG master
)
CPMAddPackage(
  NAME Camera
  GIT_REPOSITORY git@gitlab.com:signal-detection-system-coding-team/camera.git
  GIT_TAG master
)
# ---- Add source files ----

# Note: globbing sources is considered bad practice as CMake's generators may not detect new files
# automatically. Keep that in mind when changing files, or explicitly mention them here.
file(GLOB_RECURSE headers CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")
file(GLOB_RECURSE sources CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/source/*.cpp")
# ---- Create library ----

# Note: for header-only libraries change all PUBLIC flags to INTERFACE and create an interface
# target: add_library(Calsig INTERFACE)
add_library(Calsig ${headers} ${sources})

set_target_properties(Calsig PROPERTIES CXX_STANDARD 14)

# being a cross-platform target, we enforce standards conformance on MSVC
target_compile_options(Calsig PUBLIC "$<$<COMPILE_LANG_AND_ID:CXX,MSVC>:/permissive->")

# Link dependencies
ament_target_dependencies(
  Calsig geometry_msgs rclcpp rosbag2_cpp
)
target_link_libraries(
  Calsig  Eigen3::Eigen LambertGps::LambertGps Rail::Rail ${OpenCV_LIBS} Ceres::ceres
                Camera::Camera
)
# rosbag
target_link_directories(Calsig PUBLIC /opt/ros/foxy/lib)
target_link_directories(Calsig PUBLIC /opt/ros/foxy/lib/x86_64-linux-gnu)

# \rosbag

target_include_directories(
  Calsig PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
                $<INSTALL_INTERFACE:include/${PROJECT_NAME}-${PROJECT_VERSION}>
)

# ---- Create an installable target ----
# this allows users to install and find the library via `find_package()`.

# the location where the project's version header will be placed should match the project's regular
# header paths
string(TOLOWER ${PROJECT_NAME}/version.h VERSION_HEADER_LOCATION)

packageProject(
  NAME ${PROJECT_NAME}
  VERSION ${PROJECT_VERSION}
  NAMESPACE ${PROJECT_NAME}
  BINARY_DIR ${PROJECT_BINARY_DIR}
  INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include
  INCLUDE_DESTINATION include/${PROJECT_NAME}-${PROJECT_VERSION}
  VERSION_HEADER "${VERSION_HEADER_LOCATION}"
  COMPATIBILITY SameMajorVersion
  DEPENDENCIES "Ceres"
)
